<?php

$no_header = 1;
require_once( './header.inc' );

if( isset( $_SESSION[ 'user' ] ) ) {
    $race_id = $db->real_escape_string( $_REQUEST[ 'race_id' ] );

    // Create temporary table
    $temp_query = 'create temporary table race_faculty select id, first, last from election_faculty where active = 1';
    $temp_result = $db->query( $temp_query );

    // Remove nominees from it
    $noms_query = 'delete from race_faculty where id in '
        . "( select nominee from election_noms_nominees where race = \"$race_id\" )";
    $noms_result = $db->query( $noms_query );

    // Remove invalid from it
    $invalid_query = 'delete from race_faculty where id in '
        . "(select candidate from election_noms_ineligible where race = \"$race_id\" )";
    $invalid_result = $db->query( $invalid_query );

    // Create list
    print "<select class=\"form-control\" id=\"faculty\">\n";
    print "<option id=\"0\">Choose a faculty member below</option>\n";
    $fac_query = 'select id, first, last from race_faculty order by last, first';
    $fac_result = $db->query( $fac_query );
    while( $fac = $fac_result->fetch_object() ) {
        print "<option id=\"$fac->id\">$fac->first $fac->last</option>\n";
    }
    print "</select>\n";
    print "</p>\n";


}
?>