<?php

require_once( './header.inc' );

if( isset( $_SESSION[ 'user' ] ) ) {
    $race_id = $db->real_escape_string( $_REQUEST[ 'race_id' ] );

    $race_query = 'select name, noms_open_date as open, noms_close_date as close '
        . 'from election_noms_races '
        . "where id = \"$race_id\"";
    $race_result = $db->query( $race_query );
    $race = $race_result->fetch_object();

    $was_i_nominated_query = 'select * from election_noms_nominees '
        . "where race = \"$race_id\" "
        . "and nominee = \"{$_SESSION[ 'user' ]}\" "
        . 'and accepted is null';
    // print $was_i_nominated_query;
    $was_i_nominated_result = $db->query( $was_i_nominated_query );
    $was_i_nominated = $was_i_nominated_result->num_rows;

?>

    <div class="container">
        <div class="row"></div>
        
        <div class="row">
            <h1 class="col-sm-12">Nominations for <?php echo $race->name; ?></h1>
        </div>

        <div class="row">
            <p class="col-sm-12">Nominations for this race will close on <?php echo date( 'l, F j, Y \a\t g:i a', strtotime( $race->close ) ); ?>.</p>
        </div>

        <div class="row">
            <h2 class="col-sm-12">Already Nominated</h2>
        </div>

        <div class="row">
<?php
        if( $was_i_nominated == 1 ) {
?>
            <div class="alert alert-primary col-sm-12">You have been nominated in this race!  Do you accept?
                <a href="javascript:void(0)" class="alert-link" id="nomyes">Yes</a> |
                <a href="javascript:void(0)" class="alert-link" id="nomno">No</a>
            </div>
<?php
        }
?>
        </div>

        <div id="nominees"></div>

        <div class="row">
            <h2 class="col-sm-12">
                People Who Can't Be Nominated
                <small class="text-muted">
                    (<a href="javascript:void(0)"
                    data-toggle="popover" data-placement="bottom"
                    title="Why Can't Some People Be Nominated?"
                    data-trigger="focus"
                    data-content="Some people are ineligible because they're in the middle of a multi-year term.  Others can't run due to a position they already hold.">Why not?</a>)
                </small>
            </h2>
        </div>

        <div id="ineligible"></div>

        <div class="row">
            <h2 class="col-sm-12">Nominate Someone Now</h2>
        </div>

        <div id="candidates"></div>
        <div id="confirm-nomination"></div>

    </div>

    <script type="text/javascript">
    $(function(){
        var race_id = "<?php echo $race_id; ?>";

        $('[data-toggle="popover"]').popover();

        $.post('nominee-buttons.php',
            { race_id: race_id },
            function(data){
                $('div#nominees').html(data);
            }
        )

        $.get('cant-be-nominated.php',
            { race_id: race_id },
            function(data){
                $('div#ineligible').html(data);
            }
        )

        $.get('list-nominees.php',
            { race_id: race_id },
            function(data){
                $('div#candidates').html(data);
            }
        )

        $('.container').on('change','select#faculty',function(){
            var nominee_id = $(this).find('option:selected').attr('id');
            // alert(nominee_id);
            $.get('confirm-nomination.php',
                { race_id: race_id, nominee_id: nominee_id },
                function(data){
                    $('div#confirm-nomination').html(data);
                }
            )
        })
    })
    </script>

<?php

require_once( './footer.inc' );

}