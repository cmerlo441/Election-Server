<?php

$no_header = 1;
require_once( './header.inc' );

if( isset( $_SESSION[ 'user' ] ) ) {
    $race_id = $db->real_escape_string( $_REQUEST[ 'race_id' ] );
    if( isset( $_REQUEST[ 'deny_id' ] ) ) {
        $deny_id = $db->real_escape_string( $_REQUEST[ 'deny_id' ] );
    }

    $nominees_query = 'select f.first, f.last '
        . 'from election_faculty as f, election_noms_nominees as n '
        . "where n.race = \"$race_id\" "
        . 'and n.nominee = f.id '
        . 'order by f.last, f.first';
    $nominees_result = $db->query( $nominees_query );
    if( $nominees_result->num_rows == 0 )
        print "            <p>Nobody has been nominated for this race yet.</p>\n";
    else {
        print "<p>\n";
        while( $nom = $nominees_result->fetch_object() ) {
            print "            <button class=\"btn btn-outline-info\" disabled>$nom->first $nom->last</button>\n";
        }
        print "</p>\n";
    }
?>
        </div>
<?php
}
?>