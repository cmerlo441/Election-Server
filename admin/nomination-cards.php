<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {

    $year = $db->real_escape_string( $_REQUEST[ 'year' ] );
    if( $year == '' ) $year = date( 'Y' );

    $noms_races_query = 'select * from election_noms_races '
        . "where noms_open_date like \"$year%\"";
    // print $noms_query;
    $noms_races_result = $db->query( $noms_races_query );
    if( $noms_races_result->num_rows == 0 ) {
?>
    <div class="card mb-4 box-shadow">
        <div class="card-header">
            <h4 class="my-0 font-weight-normal">No Current Nominations</h4>
        </div>
        <div class="card-body">
            <h1 class="card-title pricing-card-title">No Current Nominations</h1>
            <p>There are no nominations at this time.</p>
            <!--<button type="button" class="btn btn-lg btn-block btn-primary">Contact us</button>-->
        </div>
    </div>
<?php
    } else {
        while( $race = $noms_races_result->fetch_object() ) {
            // print "$race->id";
            $nominees_count_query = 'select count(id) as count '
                . 'from election_noms_nominees '
                . "where race = $race->id";
            // print "$nominees_count_query";
            $nominees_count_result = $db->query( $nominees_count_query );
            $nominees_row = $nominees_count_result->fetch_object();
            $nominees = $nominees_row->count;
            $nominees_count_result->close();
            // print "Nominated: $nominees";

            $accepted_query = 'select count(id) as count from election_noms_nominees '
                . "where race = \"$race->id\" "
                . "and accepted is not null";
            // print "$accepted_query";
            $accepted_result = $db->query( $accepted_query );
            $accepted_row = $accepted_result->fetch_object();
            $accepted = $accepted_row->count;
            $accepted_result->close();
            // print "Accepted: $accepted";

            $end = '';
            // print "$race->noms_close_date";
            if( $race->noms_close_date < date( 'Y-m-d H:i:s' ) )
                $end = 'Nominations closed ' . date( 'n/d', strtotime( $race->noms_close_date ) );
            else {
                // $when = date('l', strtotime( $race->noms_close_date ) );
                // $today = strtotime( 'now' );
                // $tomorrow = strtotime( 'tomorrow' );
                // if( substr( $race->noms_close_date, 0, 10 ) == date( 'Y-m-d', $today ) )
                //     $when = 'today';
                // else if( substr( $race->noms_close_date, 0, 10 ) == date( 'Y-m-d', $tomorrow ) )
                //     $when = 'tomorrow';
                // $end = "Nominations close $when";
                $end = 'Nominations close ' . today_tomorrow_day( $race->noms_close_date );
            }
?>
        <div class="card mb-4 box-shadow" id="<?php echo $race->id; ?>">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">
                    <?php echo $end ?>
                    <small class="text-muted">
                        <?php echo date( 'g:i a', strtotime( $race->noms_close_date ) ); ?>
                    </small>
                </h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title"><?php echo $race->name; ?>
                </h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <li><?php echo $nominees; ?> people nominated</li>
                    <li><?php echo $accepted; ?> accepted their nominations</li>
                </ul>
                <a class="btn btn-lg btn-block btn-info nominations"
                id="<?php echo $race->id; ?>" href="nomination-race.php?id=<?php echo $race->id; ?>">
                View Settings and Nominees
                </a>
            </div>
        </div>
<?php
        }
    }
}
?>
