<?php

$no_header = 1;
require_once( '../header.inc' );
if( isset( $_SESSION[ 'admin' ] ) ) {
    $race_id = $db->real_escape_string( $_REQUEST[ 'race_id' ] );

    if( isset( $_REQUEST[ 'invalid_id' ] ) ) {
        $invalid_id = $db->real_escape_string( $_REQUEST[ 'invalid_id' ] );
        $add_query = 'insert into election_noms_ineligible '
            . "values( null, \"$race_id\", \"$invalid_id\" )";
        $add_result = $db->query( $add_query );
    }

    if( isset( $_REQUEST[ 'reinstate_id' ] ) ) {
        $reinstate_id = $db->real_escape_string( $_REQUEST[ 'reinstate_id' ] );
        $reinstate_query = 'delete from election_noms_ineligible '
            . "where race = \"$race_id\" and candidate = \"$reinstate_id\"";
        $reinstate_result = $db->query( $reinstate_query );
    }

    $cant_query = 'select f.id, f.first, f.last '
        . 'from election_noms_ineligible as i, election_faculty as f '
        . "where i.race = \"$race_id\" "
        . 'and i.candidate = f.id '
        . 'order by f.last, f.first';
    $cant_result = $db->query( $cant_query );
    if( $cant_result->num_rows == 0 ) {
        print "<p>Right now, all faculty are eligible to be nominated.</p>\n";
    } else {
        print "<p>\n";
        while( $cant = $cant_result->fetch_object() ) {
            print "<button class=\"btn btn-danger reinstate\" id=\"$cant->id\">$cant->first $cant->last</button>\n";
        }
        print "</p>\n";
    }

    print "<p>Add a faculty member to exclude from this race:\n";
    $temp_query = 'create temporary table race_faculty select id, first, last from election_faculty where active = 1';
    // print "$temp_query\n";
    $temp_result = $db->query( $temp_query );
    $temp_count_query = 'select count(id) as count from race_faculty';
    $temp_count_result = $db->query( $temp_count_query );
    $count = $temp_count_result->fetch_object();
    // print "Amount: $count->count\n";

    $noms_query = 'delete from race_faculty where id in '
        . "( select nominee from election_noms_nominees where race = \"$race_id\" )";
    // print $noms_query;
    $noms_result = $db->query( $noms_query );
    $temp_count_result = $db->query( $temp_count_query );
    // $count = $temp_count_result->fetch_object();
    // print "Amount: $count->count\n";

    $invalid_query = 'delete from race_faculty where id in '
        . "(select candidate from election_noms_ineligible where race = \"$race_id\" )";
    $invalid_result = $db->query( $invalid_query );

    print "<select class=\"form-control\" id=\"faculty\">\n";
    print "<option id=\"0\">Choose a faculty member below</option>\n";
    $fac_query = 'select id, first, last from race_faculty order by last, first';
    // print $fac_query;
    $fac_result = $db->query( $fac_query );
    while( $fac = $fac_result->fetch_object() ) {
        print "<option id=\"$fac->id\">$fac->first $fac->last</option>\n";
    }
    print "</select>\n";
    print "</p>\n";

?>

    <script type="text/javascript">
    $(function(){
        var race_id = "<?php echo $race_id; ?>";

        $('select#faculty').on('change',function(){
            var invalid = $(this).find('option:selected').attr('id');
            // console.log(id);
            // if( id > 0 )
            //     alert(id);
            $.post('cant-be-nominated.php',
                { race_id: race_id, invalid_id: invalid },
                function(data){
                    $('#invalid').html(data);
                }
            )
        })

        $('button.reinstate').on('click',function(){
            var reinstate_id = $(this).attr('id');
            $.post('cant-be-nominated.php',
                { race_id: race_id, reinstate_id: reinstate_id },
                function(data){
                    $('#invalid').html(data);
                }
            )
        })
    })
    </script>

<?php
}
?>