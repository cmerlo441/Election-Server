<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $election_id = $db->real_escape_string( $_REQUEST[ 'election_id' ] );
    $who_voted_query = 'select f.first, f.last '
        . 'from election_faculty as f, election_voted as v '
        . "where v.election = \"$election_id\" "
        . 'and v.faculty = f.id '
        . 'order by f.last, f.first';
    // print $who_voted_query;
    $who_voted_result = $db->query( $who_voted_query );
    print "<div class=\"row\"><h2 class=\"col-sm-12\">Voters</h2></div>\n";
    print "<div class=\"row\">\n";
    if( $who_voted_result->num_rows == 0 ) {
        print "<p class=\"col-sm-12\">Nobody has voted in this election yet.</p>";
    } else {
        print "<p class=\"col-sm-12 text-center\">\n";
        while( $voter = $who_voted_result->fetch_object() ) {
            print "    <button class=\"btn btn-outline-success\" disabled>$voter->first $voter->last</button>\n";
        }
        print "</p>\n";
    }
    print "</div>\n";
}
?>