<?php

require_once( '../header.inc' );

?>

        <div class="modal" tabindex="-1" role="dialog" id="edit-screen"></div>
        <div class="container">
            <div class="row">
                <h1>Voters List</h1>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div id="newVoter">
                        <h2>
                            <a href="javascript:void(0)" id="addVoter">Add a new voter</a>
                        </h2>
                        <div class="card bg-light" id="newVoterForm" style="display: none">
                            <h5 class="card-header">Add a New Voter</h5>
                            <div class="card-body">
                                <div class="form-inline">
                                    <input type="text" class="form-control" id="first" placeholder="First Name">
                                    <input type="text" class="form-control" id="last" placeholder="Last Name">
                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                    <button class="btn btn-primary" id="create">Create Voter</button>
                                    <button class="btn btn-secondary" id="cancel">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="card" id="addVoterCard">
                    <!-- <div class="col-xl-12"> -->
                        <h5 class="card-header">Search: <input type="text" id="search" class="form-control" placeholder="Search" autofocus></h5>
                    <!-- </div> -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12" id="active">
                            </div>

                            <div class="col-md-6 col-sm-12" id="inactive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function(){

                function address(first, last) {
                    if(first.trim().length > 0 && last.trim().length > 0) {
                        email = first.trim().toLowerCase().replace(/\s/g,'') + '.' + last.trim().toLowerCase().replace(/\s/g,'') + '@ncc.edu';
                        return email;
                    } else {
                        return '';
                    }
                }

                $.get('voters-table.php',
                    { active: 1 },
                    function(data){
                        $('div#active').html(data);
                    }
                );
                $.get('voters-table.php',
                    { active: 0 },
                    function(data){
                        $('div#inactive').html(data);
                    }
                );

                $('a#addVoter').click(function(){
                    $('div#newVoterForm').slideDown();
                    $('input#first').focus();
                    $(this).slideUp();
                    $('#addVoterCard').addClass('my-5');
                })

                $('input#first').keyup(function(){
                    var first = $('input#first').val();
                    var last = $('input#last').val();
                    $('input#email').val(address(first,last));
                    
                })
                $('input#last').keyup(function(){
                    var first = $('input#first').val();
                    var last = $('input#last').val();
                    $('input#email').val(address(first,last));
                    
                })

                $('button#create').click(function(){
                    var first = $('input#first').val();
                    var last = $('input#last').val();
                    var email = $('input#email').val();
                    if( first.trim().length > 0 && last.trim().length > 0 && email.trim().length > 0 ) {
                        $.post( 'add-faculty.php',
                            { first: first, last: last, email: email },
                            function(data){
                                console.log(data);
                                $.get('voters-table.php',
                                    { active: 1 },
                                    function(data){
                                        $('div#active').html(data);
                                    }
                                );
                            }
                        );
                    }
                    $('tr:contains(' + email + ')').removeClass('table-light').addClass('table-success');
                })

                $('input#search').keyup(function(){
                    var search_key = $(this).val();
                    $.get('voters-table.php',
                        { active: 1, search_key: search_key },
                        function(data){
                            $('div#active').html(data);
                        }
                    );
                    $.get('voters-table.php',
                        { active: 0, search_key: search_key },
                        function(data){
                            $('div#inactive').html(data);
                        }
                    );
                })
            })
        </script>

<?php

require_once( '../footer.inc' );
?>