<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $active = $db->real_escape_string( $_REQUEST[ 'active' ] );
    $search_key = $db->real_escape_string( $_REQUEST[ 'search_key' ] );

    $voter_query = 'select id, first, last, email '
        . 'from election_faculty '
        . "where active = $active ";
    if( isset( $search_key ) and strlen( $search_key ) >= 2 ) {
        $voter_query .= "and ( first like \"%$search_key%\" or last like \"%$search_key%\" ) ";
    }
    $voter_query .= 'order by last, first';
    // print $voter_query;
    $voter_result = $db->query( $voter_query );
    $num = $voter_result->num_rows;
    print "                    <h2>" . ( $active ? 'Active' : 'Inactive' ) . " Voters"
        . " <small>$num record" . ( $num == 1 ? '' : 's' );
    if( isset( $search_key ) and strlen( $search_key ) >= 2 ) {
        print " matching \"$search_key\"";
    }
    print "</small></h2>\n";
}

?>
                    <table class="table table-sm table-hover table-borderless">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
num_rows;
    while( $voter = $voter_result->fetch_object() ) {
        print "                            <tr class=\"table-" . ( $active ? 'light' : 'warning' ) . "\" id=\"$voter->id\">\n";
        print "                                <td>$voter->first $voter->last</td>\n";
        print "                                <td>$voter->email</td>\n";
        print "                                <td class=\"text-center\"><i class=\"fas fa-cog\" id=\"settings-$voter->id\" style=\"cursor: pointer\"></i></td>\n";
        print "                            <tr>\n";
    }
?>
                        </tbody>
                    </table>

                    <script type="text/javascript">
                        $(function(){
                            $('td > i.fa-cog').click(function(){
                                var id = $(this).attr('id').substring( $(this).attr('id').indexOf('-') + 1 );
                                $.get('edit-screen.php',
                                    { voter: id },
                                    function(data){
                                        // console.log(data);
                                        $('div#edit-screen').html(data).modal();
                                    }
                                );
                            });
                        })
                    </script>