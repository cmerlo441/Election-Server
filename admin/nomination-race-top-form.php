<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $id = $db->real_escape_string( $_REQUEST[ 'id' ] );

    if( isset( $_REQUEST[ 'name' ] ) or isset( $_REQUEST[ 'open' ] ) or isset( $_REQUEST[ 'close' ] ) ) {
        $name = $db->real_escape_string( $_REQUEST[ 'name' ] );
        $open = $db->real_escape_string( $_REQUEST[ 'open' ] );
        $close = $db->real_escape_string( $_REQUEST[ 'close' ] );

        $update_query = 'update election_noms_races '
            . "set name = \"$name\", "
            . 'noms_open_date = "' . date( 'Y-m-d G:i:s', strtotime( $open ) ) . '", '
            . 'noms_close_date = "' . date( 'Y-m-d G:i:s', strtotime( $close ) ) . '" '
            . "where id = \"$id\"";

        // print $update_query;

        $update_result = $db->query( $update_query );
        // print $db->affected_rows;
    }

    $race_query = 'select name, noms_open_date, noms_close_date '
        . 'from election_noms_races '
        . "where id = \"$id\"";
    // print "$race_query";
    $race_result = $db->query($race_query);
    $race = $race_result->fetch_object();
    $name = $race->name;
    $open = $race->noms_open_date;
    $close = $race->noms_close_date;

?>
    <div class="row">
        <h1 class="col-sm-12">Nominations for <?php echo $name; ?></h1>
    </div>

    <div class="row">
        <div class="form-group col-sm-12" id="nameGroup">
            <label for="name">Election Name</label>
            <input type="text" class="form-control" id="name" aria-describedby="nameHelp" value="<?php echo $name; ?>">
            <small id="nameHelp" class="form-text text-muted">A descriptive name for the election</small>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-6">
            <label for="open" class="col-sm-4 col-form-label">Opening Time</label>
            <div class="col-sm-12 input-group date" id="open" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#open" value="<?php echo date('m/j/Y h:i A', strtotime($open)); ?>" />
                <div class="input-group-append" data-target="#open" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>

        <div class="form-group col-sm-6">
            <label for="close" class="col-sm-4 col-form-label">Closing Time</label>
            <div class="col-sm-12 input-group date" id="close" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input" data-target="#close" value="<?php echo date('m/j/Y h:i A', strtotime($close)); ?>" />
                <div class="input-group-append" data-target="#close" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="commitButtonRow" style="display: none">
        <button type="button" class="btn btn-info disabled" id="commit">Commit Changes</button>
    </div>

    <script type="text/javascript">
    $(function(){
        var id = "<?php echo $id; ?>";
        var origName = "<?php echo $name; ?>";
        var origOpen = "<?php echo date( 'm/d/Y g:i A', strtotime( $open ) ); ?>";
        var origClose = "<?php echo date( 'm/d/Y g:i A', strtotime( $close ) ); ?>";

        function detectChange() {
            if (origName != $('#name').val() || origOpen != $('#open > input').val() || origClose != $('#close > input').val() ) {
                $('div#commitButtonRow').slideDown();
                $('button#commit').removeClass('disabled');
            } else {
                $('button#commit').addClass('disabled');
                $('div#commitButtonRow').slideUp();
            }
        }

        $('#open').datetimepicker();
        $('#close').datetimepicker();

        $('#name').on('keyup', function(){
            detectChange();
        })
        $('#open > input').on('keyup', function() {
            detectChange();
        })
        $('#open').on('change.datetimepicker', function() {
            detectChange();
        })

        $('#close > input').on('keyup', function() {
            detectChange();
        })
        $('#close').on('change.datetimepicker', function() {
            detectChange();
        })

        $('button#commit').on('click',function(){
            var name = $('#name').val();
            var open = $('#open > input').val();
            var close = $('#close > input').val();
            $.post('nomination-race-top-form.php',
                {
                    id: id,
                    name: name,
                    open: open,
                    close: close
                },
                function(data){
                    $('#top-form').html(data);
                }
            )
        })
    })
    </script>
<?php
}
?>