<?php

$datepick = 1;
require_once('../header.inc');

if (isset($_SESSION['admin'])) {
    $id = $db->real_escape_string($_REQUEST['id']);
?>

    <div class="container">
        <div id="modal-landing"></div>

        <div id="top-form"></div>

        <!-- ################################ Invalid ############################# -->

        <div class="row">
            <h2 class="col-sm-12">Faculty who Can't Be Nominated</h2>
            <div class="col-sm-12" id="invalid"></div>
        </div>



        <!-- ################################ Nominees ############################# -->

        <div id="noms-table"></div>

    </div>

    <script type="text/javascript">
        $(function() {

            var id = "<?php echo $id; ?>";

            $.get('nomination-race-top-form.php',
                { id: id },
                function(data){
                    $('#top-form').html(data);
                }
            )

            $.get('cant-be-nominated.php',
                { race_id: id },
                function(data){
                    $('#invalid').html(data);
                }
            )

            $.get('nomination-race-nominees-table.php',
                { id: id },
                function(data){
                    $('#noms-table').html(data);
                }
            )
        })
    </script>
<?php
}
require_once('../footer.inc');
?>