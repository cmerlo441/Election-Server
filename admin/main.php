<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {

    $this_year = date( 'Y' );

    $first_year_query = 'select starttime from election_elections '
        . 'where starttime > "0000" '
        . 'order by starttime limit 1';
    $first_year_result = $db->query( $first_year_query );
    $first_year_row = $first_year_result->fetch_object();
    $first_year = substr( $first_year_row->starttime, 0, 4 );
    $years = array();
    for( $i = $this_year; $i >= $first_year; --$i ) {
        $years[] = $i;
    }

?>
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h1 class="display-4">Election Administration</h1>
            <p class="lead">Choose an election year:</p>
            <select class="form-control" id="year">
<?php
    foreach( $years as $year ) {
        print "                <option id=\"$year\">$year</option>\n";
    }
?>
            </select>
        </div>

        <div class="container">
            <div class="card-deck mb-3 text-center" id="cards">
                <script type="text/javascript">
                $(function(){
                    $('body').css('background-color','#ccc');
                    $.get('election_cards.php',
                        function(data){
                            $('div#cards').html(data);
                        }
                    )
                })

                $('select#year').change(function(){
                    var year = $(this).val();
                    $.get('election_cards.php',
                        { year: year },
                        function(data){
                            $('div#cards').html(data);
                        }
                    );
                })
                </script>
            </div>
        </div>

        <div class="container">
            <div class="card-deck mb-3 text-center" id="noms">
                <script type="text/javascript">
                $(function(){
                    $.get('nomination-cards.php',
                        function(data){
                            $('div#noms').html(data);
                        }
                    )
                })
                </script>
            </div>
        </div>
<?php
}
?>