<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $id = $db->real_escape_string($_REQUEST['id']);

    if( isset( $_REQUEST[ 'removeid' ] ) ) {
        $removeId = $db->real_escape_string( $_REQUEST[ 'removeid' ] );
        $remove_query = 'delete from election_noms_nominees '
            . "where id = \"$removeId\"";
        $remove_result = $db->query( $remove_query );
    }
?>
        <div class="row">
            <h2 class="col-sm-12">Nominees</h2>
        </div>

        <div class="row">
            <?php
            $nominees_query = 'select f1.first as nomFirst, f1.last as nomLast, f1.id as facId, '
                . 'n.id as nomId, n.accepted, '
                . 'f2.first as byFirst, f2.last as byLast '
                . 'from election_faculty as f1, election_faculty as f2, election_noms_nominees as n '
                . "where n.race = \"$id\" "
                . 'and n.nominee = f1.id '
                . 'and n.nominated_by = f2.id '
                . 'order by f1.last, f1.first';
            // print "$nominees_query";
            $nominees_result = $db->query($nominees_query);
            ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nominee</th>
                        <th>Nominated By</th>
                        <th>Accepted?</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($nom = $nominees_result->fetch_object()) {
                        $acc = $nom->accepted == '' ? 'Not yet' : date( 'n/j g:i a', strtotime( $nom->accepted ) );
                        print "                    <tr class=\"" . ( $acc == 'Not yet' ? 'table-warning' : 'table-success' ) . "\" id=$nom->nomId>\n";
                        print "                        <td class=\"nominee\">$nom->nomFirst $nom->nomLast</td>\n";
                        print "                        <td class=\"nominatedBy\">$nom->byFirst $nom->byLast</td>\n";
                        print "                        <td class=\"accepted\">$acc</td>\n";
                        print "                        <td><button type=\"button\" class=\"btn btn-danger remove\">&times</button></td>\n";
                        print "                    </tr>\n";
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <script type="text/javascript">
        $(function(){
            $('button.remove').on('click', function(){
                var id = "<?php echo $id; ?>";
                var nominee_id = $(this).parent().parent().attr('id');

                $.get('remove-nominee-modal.php',
                    { id: id, nominee_id: nominee_id },
                    function(data){
                        $('div#modal-landing').html(data);
                        $('div#removeModal').modal();
                    }
                )
            })
        })
        </script>
<?php
}
?>