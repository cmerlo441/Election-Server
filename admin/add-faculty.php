<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $first = $db->real_escape_string( $_REQUEST[ 'first' ] );
    $last = $db->real_escape_string( $_REQUEST[ 'last' ] );
    $email = $db->real_escape_string( $_REQUEST[ 'email' ] );

    $login = strtolower( substr( $first, 0, 6 ) . substr( $last, 0, 1 ) );

    $dupe_query = 'select * from election_faculty '
        . "where email like \"%$email%\"";
    $dupe_result = $db->query( $dupe_query );
    if( $dupe_result->num_rows == 0 ) {
        $insert_query = 'insert into election_faculty( id, first, last, email, password, active ) '
            . "values( null, \"$first\", \"$last\", \"$email\", \"no password\", 1 )";
        // print $insert_query;
        $insert_result = $db->query( $insert_query );
    }
    print $db->affected_rows;
}