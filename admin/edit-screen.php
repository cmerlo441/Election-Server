<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $id = $db->real_escape_string( $_REQUEST[ 'voter' ] );
    $fac_query = 'select first, last, active '
        . 'from election_faculty '
        . "where id = \"$id\"";
    $fac_result = $db->query( $fac_query );
    if( $fac_result->num_rows == 1 ) {
        $fac = $fac_result->fetch_object();
        $fac_result->close();
?>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Voter Record</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" role="alert">
                    <strong>Note:</strong>  Your changes will take effect as you type.
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" id="good">
                    Your changes have been saved.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-warning alert-dismissible fade show" role="alert" id="bad">
                    <strong>Something went wrong.</strong>  Your changes were not saved.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form">
                    <div class="form-group">
                        <label for="first">First Name</label>
                        <input type="text" class="form-control onchange" id="first" placeholder="First Name" value="<?php echo $fac->first; ?>">
                    </div>
                </div>
                <div class="form">
                    <div class="form-group">
                        <label for="last">Last Name</label>
                        <input type="text" class="form-control onchange" id="last" placeholder="=Last Name" value="<?php echo $fac->last; ?>">
                    </div>
                </div>
                <div class="form">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control onchange" id="email" placeholder="Email" value="<?php echo $fac->email; ?>">
                    </div>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="active"<?php if( $fac->active == 1 ) echo ' checked'; ?>>
                    <label class="form-check-label" for="active">Active Voter</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            var id = "<?php echo $id; ?>";

            $('div#good').hide();
            $('div#bad').hide();

            function update(){
                $.get('voters-table.php',
                    { active: 1 },
                    function(data){
                        $('div#active').html(data);
                    }
                );
                $.get('voters-table.php',
                    { active: 0 },
                    function(data){
                        $('div#inactive').html(data);
                    }
                );
            }

            $('input.onchange').keyup(function(){
                $('div#good').stop();
                $('div#bad').stop();

                var field = $(this).attr('id');
                var value = $(this).val();

                $.post('edit-faculty.php',
                    { id: id, field: field, value: value },
                    function(data){
                        // console.log(data);
                        if(data == '1') {
                            $('div#good').slideDown(function(){
                                $('div.alert').delay(2000).slideUp();
                            });
                        } else {
                            $('div#bad').slideDown(function(){
                                $('div.alert').delay(2000).slideUp();
                            });
                        }
                        update();
                    }
                );
            })

            $('input#active').change(function(){
                var active = $(this).is(':checked') ? 1 : 0;
                $.post('edit-faculty.php',
                    { id: id, field: 'active', value: active },
                    function(data){
                        if(data == '1') {
                            $('div#good').slideDown(function(){
                                $('div.alert').delay(2000).slideUp();
                            });
                        } else {
                            $('div#bad').slideDown(function(){
                                $('div.alert').delay(2000).slideUp();
                            });
                        }
                        update();
                    }
                );
            })
        })
    </script>
<?php
    } else {
?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Voter Not Found</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>The voter you're looking for isn't here.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
<?php
    }
}
?>
