<?php

$no_header = 1;
require_once( '../header.inc' );

if( isset( $_SESSION[ 'admin' ] ) ) {
    $id = $db->real_escape_string( $_REQUEST[ 'id' ] );
    $nominee_id = $db->real_escape_string( $_REQUEST[ 'nominee_id' ] );
    $nom_query = 'select r.name, n.accepted, f.first, f.last '
        . 'from election_noms_races as r, election_noms_nominees as n, election_faculty as f '
        . "where n.id = \"$nominee_id\" "
        . 'and n.race = r.id '
        . 'and n.nominee = f.id';
    $nom_result = $db->query( $nom_query );
    $nom = $nom_result->fetch_object();
    // print_r( $nom );
?>
        <div class="modal fade" id="removeModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Are You Sure?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to remove the nomination for <?php echo "$nom->first $nom->last"; ?>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger confirm-remove">Yes, Remove the Nomination</button>
                </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        $(function(){
            var id = "<?php echo $id; ?>";
            var nominee_id = "<?php echo $nominee_id; ?>";

            $('button.confirm-remove').on('click', function(){
                $.post('nomination-race-nominees-table.php',
                    { id: id, removeid: nominee_id },
                    function(data){
                        $('#noms-table').html(data);
                        $('div#removeModal').modal('hide');
                    }
                )
            })
        })
        </script>

<?php
}