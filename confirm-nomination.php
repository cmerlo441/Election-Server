<?php

$no_header = 1;
require_once( './header.inc' );

if( isset( $_SESSION[ 'user' ] ) ) {
    $race_id = $db->real_escape_string( $_REQUEST[ 'race_id' ] );
    $nominee_id = $db->real_escape_string( $_REQUEST[ 'nominee_id' ] );
    $confirmed = 0;
    if( isset( $_REQUEST[ 'confirm' ] ) )
        $confirmed = 1;

    if( $nominee_id == $_SESSION[ 'user' ] ) {

        // User nominated him/her self

        $nom_query = 'insert into election_noms_nominees '
            . ' ( race, nominee, nominated_by, nominated_at, accepted ) '
            . "values( \"$race_id\", \"$nominee_id\", \"$nominee_id\", "
            . '"' . date( 'Y-m-d H:i:s' ) . '", "' . date( 'Y-m-d H:i:s' ) . '" )';
        $nom_result = $db->query( $nom_query );
?>
    <div class="alert alert-success alert-dismissible fade show" id="goodluck" style="display: none">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="alert-heading">Good Luck!</h4>
        <hr>
        <p>We have recorded your self-nomination.  Don't forget to vote for yourself!</p>
    </div>
    <script type="text/javascript">
        $(function(){
            var race_id = "<?php echo $race_id; ?>";
            $('div#goodluck').slideDown();

            $.post('nominee-buttons.php',
                { race_id: race_id },
                function(data){
                    $('div#nominees').html(data);
                }
            )

        })
    </script>
<?php
    } else {

        // User nominated someone else

        $race_query = 'select name from election_noms_races '
            . "where id = \"$race_id\"";
        $race_result = $db->query( $race_query );
        $race_row = $race_result->fetch_object();
        $race = $race_row->name;
        $race_result->close();

        $name_query = 'select first, last, email from election_faculty '
            . "where id = \"$nominee_id\"";
        $name_result = $db->query( $name_query );
        $name_row = $name_result->fetch_object();
        $name = "$name_row->first $name_row->last";
        $email = $name_row->email;
        $name_result->close();

        // They might be here to confirm their choice

        if( $confirmed == 1 ) {
            $nom_query = 'insert into election_noms_nominees '
                . ' ( race, nominee, nominated_by, nominated_at, accepted ) '
                . "values( \"$race_id\", \"$nominee_id\", \"{$_SESSION[ 'user' ]}\", "
                . '"' . date( 'Y-m-d H:i:s' ) . '", null )';
            // print $nom_query;
            $nom_result = $db->query( $nom_query );

            /*
             * TODO
             * 
             * WHY won't this div appear?  I see it load in in the web console,
             * but it will NOT appear on the screen
             * Going to have to use the appearance of the new candidate button
             * as confirmation for now
             * Grrr
             */

             // Send some email
             $to = "$name <$email>";
             $subject = "You Have Been Nominated";
             $message = "Someone has nominated you to serve on $race.  "
                . "Please visit http://www.matcmp.ncc.edu/~voter/ and sign in to accept your nomination.";
             $header = "Content-Type: text/html; charset=UTF-8\n"
                . "From: MAT/CSC/ITE Election Server <christopher.merlo@ncc.edu>";

             mail( $to, $subject, $message, $header );
?>
    <div class="alert alert-success alert-dismissable fade show" id="confirmed" >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="alert-heading">Your Nomination Has Been Confirmed</h4>
        <hr>
        <p>
            Thank you for nominating <?php echo $name; ?>.
        </p>
    </div>
    <script type="text/javascript">
        $(function(){
            var race_id = "<?php echo $race_id; ?>";
            var nominee_id = "<?php echo $nominee_id; ?>";

            $.post('nominee-buttons.php',
                { race_id: race_id },
                function(data){
                    $('div#nominees').html(data);
                }
            )
            $.get('list-nominees.php',
                { race_id: race_id },
                function(data){
                    $('div#candidates').html(data);
                }
            )
            $('div#confirmed').slideDown();
        })
    </script>
<?php
        }

        // Otherwise, make sure that's what they want to do

        else {

?>
    <div class="alert alert-warning alert-dismissable fade show" id="confirm" style="display: none">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="alert-heading">Confirm Your Nomination</h4>
        <hr>
        <p>
            Do you want to nominate <strong><?php echo $name; ?></strong> in this race?
            <a href="javascript:void(0)" class="alert-link" id="confirm">Yes</a> |
            <a href="javascript:void(0)" class="alert-link" id="cancel">No</a>
        </p>
    </div>
    <script type="text/javascript">
        $(function(){
            var race_id = "<?php echo $race_id; ?>";
            var nominee_id = "<?php echo $nominee_id; ?>";

            $('div#confirm').slideDown();
            $('a#confirm').on('click',function(){
                $('div#confirm').slideUp();

                $.post('confirm-nomination.php',
                    {
                        race_id: race_id,
                        nominee_id: nominee_id,
                        confirm: 1
                    },
                    function(data){
                        $.post('nominee-buttons.php',
                            { race_id: race_id },
                            function(data){
                                $('div#nominees').html(data);
                            }
                        )
                        $.get('list-nominees.php',
                            { race_id: race_id },
                            function(data){
                                $('div#candidates').html(data);
                            }
                        )
                    }
                )
            })
        })
    </script>
<?php

    } // else we had to confirm the nomination choice



    }

}
?>