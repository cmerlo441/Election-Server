<?php
include_once( 'Faculty.php' );
require_once( ".htpasswd" );

// $version = "1.0.1";
require_once( 'version.php' );

$ten_minutes_ago = date( 'Y-m-d H:i:s', mktime( date('H'), date('i') - 10 ) );
$clear_forgot_query = 'delete from  election_forgot_pw '
    . "where timestamp < \"$ten_minutes_ago\"";
$db->query( $clear_forgot_query );

$docroot = "/~voter";

session_start();
if( isset( $_REQUEST[ 'signout' ] ) ) {
    if( isset( $_SESSION[ 'user' ] ) )
        unset( $_SESSION[ 'user' ] );
    else if( isset( $_SESSION[ 'admin' ] ) )
        unset( $_SESSION[ 'admin' ] );
}

function today_tomorrow_day( $date /* Y-m-d H:i:s */, $caps = 0 ) {
    if( substr( $date, 0, 10 ) == date( 'Y-m-d', strtotime( 'now' ) ) )
        return $caps == 1 ? 'Today' : 'today';
    else if( substr( $date, 0, 10 ) == date( 'Y-m-d', strtotime( 'tomorrow' ) ) )
        return $caps == 1 ? 'Tomorrow' : 'tomorrow';
    else
        return date('l', strtotime( $date ) );
    }

if( !isset( $no_header ) ) {
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="icon" href="../../../../favicon.ico">-->
<?php

    $title_string = "MAT/CSC/ITE Elections";
    if( isset( $title ) )
        $title_string .= " :: $title";

?>

        <title><?php echo $title_string; ?></title>

        <!-- Bootstrap core CSS -->
        <!-- <link rel="stylesheet" href="<?php echo $docroot; ?>/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel="stylesheet" href="<?php echo $docroot; ?>/css/sticky-footer.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<?php
    if( isset( $pricing ) )
        print "        <link href=\"$docroot/css/pricing.css\" rel=\"stylesheet\">\n";
    if( isset( $floating_labels ) )
        print "        <link href=\"$docroot/css/floating-labels.css\" rel=\"stylesheet\">\n";
    if( isset( $signin ) )
        print "        <link href=\"$docroot/css/signin.css\" rel=\"stylesheet\">\n";
    if( isset( $datepick ) ) {
        print "          <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css\" />\n";
    }
?>

        <!-- Javascript -->
        <script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<?php
    if( isset( $datepick ) ) {
        print "        <script type=\"text/javascript\" src=\"$docroot/js/moment.js\"></script>\n";
        print "        <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js\"></script>\n";
    }
    if( isset( $sortable ) ) {
        print "            <script src=\"https://SortableJS.github.io/Sortable/Sortable.js\"></script>\n";
    }

    print "            <script src=\"https://unpkg.com/@popperjs/core@2\"></script>\n";
?>

    <!-- FontAwesome -->
    <script src="https://use.fontawesome.com/eee8367865.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

       <style>
       @media print {
      div.d-flex{
	none;
      }
    }
    </style>

    </head>

    <body>
        <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
            <h5 class="my-0 mr-md-auto font-weight-normal">NCC MAT/CSC/ITE Elections</h5>
<?php
    if( isset( $_SESSION[ 'user' ] ) ) {
?>
            <nav class="my-2 my-md-0 mr-md-3">
                <a class="p-2 text-dark" href="<?php echo $docroot; ?>/about.php">About This Site</a>
            </nav>
            <a class="m-1 btn btn-outline-danger signout" href="?signout=1">Sign Out</a>
            <a class="m-1 btn btn-outline-primary" href="<?php echo $docroot; ?>/profile.php"><?php echo $_SESSION[ 'name' ]; ?></a>
<?php
    } else if( isset( $_SESSION[ 'admin' ] ) ) {
?>
            <nav class="my-2 my-md-0 mr-md-3">
                <a class="p-2 text-dark" href="<?php echo $docroot; ?>/admin/logins.php">Login History</a>
                <a class="p-2 text-dark" href="<?php echo $docroot; ?>/admin/voters.php">Voters List</a>
                <a class="p-2 text-dark" href="<?php echo $docroot; ?>/admin/create.php">Create an Election</a>
            </nav>
            <a class="m-1 btn btn-outline-danger" href="<?php echo $docroot; ?>/admin/">Admin Home</a>
            <a class="m-1 btn btn-danger signout" href="#">Sign Out</a>
<?php
    } else {
?>
            <nav class="my-2 my-md-0 mr-md-3">
                <a class="p-2 text-dark" href="<?php echo $docroot; ?>/about.php">About This Site</a>
                <a class="p-2 text-dark" href="<?php echo $docroot; ?>/forgot_password.php">Forgot Your Password?</a>
            </nav>
<?php
    }
?>
        </div>
        <script type="text/javascript">
        $(function(){
            $('a.signout').on('click',function(){
                location.href = location.href + "?signout=1";
            })
        })
        </script>

<?php
} // no header
?>
