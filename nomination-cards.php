<?php

$no_header = 1;
require_once( './header.inc' );

$noms_races_query = 'select id, name, noms_open_date, noms_close_date '
    . 'from election_noms_races '
    . "where noms_open_date < \"" . date( 'Y-m-d H:i:s' )
    . "\" and noms_close_date > \"" . date( 'Y-m-d H:i:s' ) . "\" "
    . 'order by noms_close_date, name';
// print "$noms_races_query";
$noms_races_result = $db->query( $noms_races_query );

if( $noms_races_result->num_rows == 0 ) {
    ?>
    <div class="card mb-4 box-shadow">
        <div class="card-header">
            <h4 class="my-0 font-weight-normal">No Open Nominations</h4>
        </div>
        <div class="card-body">
            <h1 class="card-title pricing-card-title">No Open Nominations</h1>
            <p>There are no nominations taking place at this time.</p>
        </div>
    </div>
<?php
} else {
    while( $race = $noms_races_result->fetch_object() ) {
        $id = $race->id;
        $name = $race->name;
        $open = $race->noms_open_date;
        $close = $race->noms_close_date;

        $nom_count_query = 'select count(id) as count '
            . 'from election_noms_nominees '
            . "where race = \"$id\"";
        $nom_count_result = $db->query( $nom_count_query );
        $nom_count_row = $nom_count_result->fetch_object();
        $nom_count = $nom_count_row->count;
        $nom_count_result->close();

?>
        <div class="card mb-4 box-shadow border-info" id="<?php echo $id; ?>">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">
                    Nominations Close <?php echo today_tomorrow_day( $close, 1 ); ?>
                    <small class="text-muted">
                        <?php echo date( 'g:i a', strtotime( $close ) ); ?>
                    </small>
                </h4>
            </div>

            <div class="card-body">
<?php
        $was_i_nominated_query = 'select * from election_noms_nominees '
            . "where race = \"$id\" "
            . "and nominee = \"{$_SESSION[ 'user' ]}\" "
            . 'and accepted is null';
        // print $was_i_nominated_query;
        $was_i_nominated_result = $db->query( $was_i_nominated_query );
        $was_i_nominated = $was_i_nominated_result->num_rows;
        if( $was_i_nominated == 1 ) {
?>
                <div class="alert alert-warning nominated" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading">You've Been Nominated!</h4>
                    <p>
                        Click here to <a href="javascript:void(0)" class="alert-link accept_nomination">accept</a> or
                        <a href="javascript:void(0)" class="alert-link reject_nomination">reject</a> this nomination.
                    </p>
                </div>
<?php
        }
?>
                <h1 class="card-title pricing-card-title"><?php echo $name; ?>
                </h1>
                <?php echo $nom_count . ( $nom_count == 1 ? ' person has': ' people have' )
                    . ' been nominated.'; ?>

                <button type="button" class="btn btn-lg btn-block btn-info nominate"
                    id="<?php echo $id; ?>">
                    Nominate Someone Now!
                </button>
            </div>
        </div>

        <script type="text/javascript">
        $(function(){
            $('button.nominate').on('click', function(){
                var id = $(this).attr('id');
                var form = document.createElement('form');
                document.body.appendChild(form);
                form.method = 'post';
                form.action = 'nominate.php';
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = 'race_id';
                input.value = id;
                form.appendChild(input);
                form.submit();
            })

            $('a.accept_nomination').on('click',function(){
                var id = $(this).closest('.card').attr('id');
                var theAlert = $(this).closest('.card').find('.nominated');

                console.log(theAlert);

                $.post('accept-nomination.php',
                    { race_id: id },
                    function(data){
                        if(data == "1" )
                            $(theAlert).slideUp();
                    }
                )
            })

        })
        </script>
<?php
    }
}