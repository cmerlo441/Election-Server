<?php

$no_header = 1;
require_once( './header.inc' );

if( isset( $_SESSION[ 'user' ] ) ) {
    $name = $db->real_escape_string( $_REQUEST[ 'name' ] );
?>
            <div class="toast" data-autohide="false" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <img src="..." class="rounded mr-2" alt="...">
                    <strong class="mr-auto">You've Been Nominated!</strong>
                    <small class="text-muted">just now</small>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    You have been nominated to serve on <?php echo $name; ?>!
                </div>
            </div>
<?php

}
?>