<?php

$no_header = 1;
require_once( './header.inc' );

if( isset( $_SESSION[ 'user' ] ) ) {
    $race_id = $db->real_escape_string( $_REQUEST[ 'race_id' ] );
    $cant_query = 'select f.id, f.first, f.last '
        . 'from election_noms_ineligible as i, election_faculty as f '
        . "where i.race = \"$race_id\" "
        . 'and i.candidate = f.id '
        . 'order by f.last, f.first';
    $cant_result = $db->query( $cant_query );
    if( $cant_result->num_rows == 0 ) {
        print "<p>Right now, all faculty are eligible to be nominated.</p>\n";
    } else {
        print "<p>\n";
        while( $cant = $cant_result->fetch_object() ) {
            print "<button class=\"btn btn-outline-danger\" disabled>$cant->first $cant->last</button>\n";
        }
        print "</p>\n";
    }

}
?>